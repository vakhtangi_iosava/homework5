package com.example.homework3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var run: TextView
    lateinit var swim: TextView
    lateinit var calories: TextView
    lateinit var average: TextView
    lateinit var averageCal: TextView
    lateinit var averageSwim: TextView
    lateinit var sumRun: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        run = findViewById(R.id.txtInput)
        swim = findViewById(R.id.txtInput2)
        calories = findViewById(R.id.txtInput3)
        average = findViewById(R.id.txtInput6)
        averageSwim = findViewById(R.id.txtInput7)
        averageCal = findViewById(R.id.txtInput8)
        sumRun = findViewById(R.id.txtInput5)

        val runSum = App.instance.db.getRSCDao().getSum()?.toString()
        sumRun.append("გარბენილი მანძილის ჯამი - $runSum")
        val getAverage = App.instance.db.getRSCDao().getAverage()?.toString()
        average.append("გარბენილი მანძილის საშუალო - $getAverage")
        val getAverageSwim = App.instance.db.getRSCDao().getAverageSwim()?.toString()
        averageSwim.append("გაცურული მანძილის საშუალო - $getAverageSwim")
        val getAverageCalories = App.instance.db.getRSCDao().getAverageCalories()?.toString()
        averageCal.append("მიღებული კალორიის საშუალო - $getAverageCalories")
    }

    fun onSave(view: View) {
        if (TextUtils.isEmpty(run.text)){
            run.setError("Run is Required")
        } else if (TextUtils.isEmpty(swim.text)){
            swim.setError("Swim is Required")
        } else if (TextUtils.isEmpty(calories.text)){
            calories.setError("Calories is Required")
        } else{
            App.instance.db.getRSCDao().insert(RunSwimCalories(0, run.text.toString().toFloat(), swim.text.toString().toFloat(), calories.text.toString().toFloat() ))
            App.instance.db.getRSCDao().getAll()?.forEach { rsc -> Log.d("MyData", rsc.toString())  }
        }
    }
}