package com.example.homework3

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [RunSwimCalories::class], version = 1)
abstract class AppDatabase :RoomDatabase() {
    abstract fun getRSCDao(): RSCDao
}