package com.example.homework3

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface RSCDao {

    @Query("SELECT * FROM rsc")
    fun getAll(): List<RunSwimCalories>?

    @Query("SELECT SUM(RUN) AS run FROM RSC")
    fun getSum(): Float?

    @Query("SELECT AVG(RUN) as run FROM RSC")
    fun getAverage(): Float?

    @Query("SELECT AVG(SWIM) as swim FROM RSC")
    fun getAverageSwim(): Float?

    @Query("SELECT AVG(CALORIES) as calories FROM RSC")
    fun getAverageCalories(): Float?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg rsc: RunSwimCalories)

    @Delete
    fun delete(rsc : RunSwimCalories)

    @Query("DELETE FROM rsc")
    fun deleteAll()
}