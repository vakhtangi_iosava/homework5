package com.example.homework3

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RSC")
data class RunSwimCalories (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,

    @ColumnInfo(name = "RUN")
    val run: Float?,

    @ColumnInfo(name = "SWIM")
    val swim: Float?,

    @ColumnInfo(name = "CALORIES")
    val calories: Float?

)